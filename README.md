# How to Change Hostname on Ubuntu/Linux Mint (from Command Line)

Open a terminal.

    sudo hostname _new hostname_


This changes the hostname temporarily.

To persist your change, edit the hostname entry in `/etc/hostname` and `/etc/hosts`.

